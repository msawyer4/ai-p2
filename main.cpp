/**
 *  In negative log space
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

int main (int argc, char **argv) {
  cout<<fixed<<setprecision(6);

  if (argc != 6) {
    cerr<<"USAGE:"<<endl;
    cerr<<"./em observations.txt transition.txt sensory.txt original.txt k";
    cerr<<endl;
    cerr<<"where k is an integer, the number of iterations to run the EM.";
    return 1;
  }

  // IO Streams
  ofstream output_file;
  ifstream observations_f;
  ifstream transitions_f;
  ifstream sensory_f;
  ifstream original_f;

  // Open Inputs
  output_file.open("output.txt");
  observations_f.open(argv[1]);
  transitions_f.open(argv[2]);
  sensory_f.open(argv[3]);
  original_f.open(argv[4]);
  int iter = atoi(argv[5]);

  double transition_log[3][3];
  double transition[3][3];
  double sensory[3][2];
  vector<char> observations;
  vector<int> og, original;
  char observation;

  for (int i = 0; observations_f.good(); i++) {
    observation = observations_f.get();
    if (observation == 'H' || observation == 'T')
      observations.push_back(observation);
  }

  // Original
  for (int i=0; original_f.good(); i++) {
    char state = original_f.get();
    if (state == 'B')
      og.push_back(0);
    else if (state == 'L')
      og.push_back(1);
    else if (state == 'M')
      og.push_back(2);
  }
//  observations_f >> observations;
  double transition_value;
  for (int i=0; transitions_f >> transition_value; i++) {
    if ((i%3)==0)
      cout<<endl;
    transition[i/3][i%3] = transition_value;
    transition_log[i/3][i%3] = -log2(transition_value);
    cout<<transition_value<<" ("<<transition_log[i/3][i%3]<<")\t";
  }
  double sensory_value;
  cout<<endl;
  for (int i=0; sensory_f >> sensory_value; i++) {
    sensory[i][0] = -log2(sensory_value);
    sensory[i][1] = -log2(1-sensory_value);
    cout<<sensory[i][0]<<" "<<sensory[i][1]<<endl;
  }
  for (int i=0; i < observations.size(); i++) {
    cout<<observations[i]<<endl;
  }
  cout<<observations.size();

  cout<<endl<<endl;

  // Initialization
  int backtrack[3][observations.size()+1];
  double vit[3][observations.size()+1];
  vit[0][0] = -log2(1.0/3.0);
  vit[1][0] = -log2(1.0/3.0);
  vit[2][0] = -log2(1.0/3.0);
  for (int i=1; i<=observations.size(); i++) {
    for (int j=0; j<3; j++) {
      double min, kurrent, min_back=0;
      for (int k=0; k<3; k++) {
        if (k==0)
          min = vit[k][i-1] + transition_log[j][k];
        kurrent = vit[k][i-1] + transition_log[j][k];
        if (kurrent < min) {
          min = kurrent;
          min_back = k;
        }
      }
      if (observations[i-1]=='H') {
        min += sensory[j][0];
      } else {
        min += sensory[j][1];
      }
      vit[j][i] = min;
      cout<<"Min_back: "<<min_back<<"\t";
      backtrack[j][i]=min_back;
    }
    cout<<endl;
  }
  for (int i=0; i<=observations.size(); i++) {
    for (int j=0; j<3; j++) {
      cout<<vit[j][i]<<' ';
    }
    cout<<endl;
  }
  cout<<endl;
  int minimum_end = 0;
  for (int i=1; i<3; i++) {
    if (vit[i][observations.size()] < vit[minimum_end][observations.size()])
      minimum_end = i;
  }
  int forward=minimum_end;
  vector <int> backtrack_v;
  vector <int> fortrack;
  backtrack_v.push_back(minimum_end);
  for (int i=observations.size(); i>0 ; i--) {
    forward = backtrack[forward][i];
    backtrack_v.push_back(forward);
  }
  for (int i=1; i<=observations.size(); i++) {
    cout<<backtrack_v.back()<<' ';
    fortrack.push_back(backtrack_v.back());
    backtrack_v.pop_back();
  }
  fortrack.push_back(minimum_end);
  cout<<fortrack.back();
  cout<<endl<<endl;

  // Iteration:
  for (int i=0; i<iter; i++) {
    cout<<endl<<"Iteration #"<<i+1<<endl;
    original = fortrack;
    // Expectation Step
    // --> Count transitions
    int BtoB=0,BtoL=0,BtoM=0,LtoB=0,LtoL=0,LtoM=0,MtoB=0,MtoL=0,MtoM=0;
    for (int i=0; i<fortrack.size()-1; i++) {
      if (fortrack[i] == 0) {
        if (fortrack[i+1] == 0)
          BtoB++;
        else if (fortrack[i+1] == 1)
          BtoL++;
        else if (fortrack[i+1] == 2)
          BtoM++;
      } else if (fortrack[i] == 1) {
        if (fortrack[i+1] == 0)
          LtoB++;
        else if (fortrack[i+1] == 1)
          LtoL++;
        else if (fortrack[i+1] == 2)
          LtoM++;
      } else if (fortrack[i] == 2) {
        if (fortrack[i+1] == 0)
          MtoB++;
        else if (fortrack[i+1] == 1)
          MtoL++;
        else if (fortrack[i+1] == 2)
          MtoM++;
      }
    }
  
    // --> Update Transition Model w/ Laplacian smoothed values
    cout<<endl<<"Transition probabilities learned:"<<endl;
    transition_log[0][0] = -log2((BtoB+1.0)/(BtoB+BtoL+BtoM+3.0));
    cout<<pow(2,-transition_log[0][0])<<'\t';
    transition_log[0][1] = -log2((LtoB+1.0)/(LtoB+LtoL+LtoM+3.0));
    cout<<pow(2,-transition_log[0][1])<<'\t';
    transition_log[0][2] = -log2((MtoB+1.0)/(MtoB+MtoL+MtoM+3.0));
    cout<<pow(2,-transition_log[0][2])<<endl;
    transition_log[1][0] = -log2((BtoL+1.0)/(BtoB+BtoL+BtoM+3.0));
    cout<<pow(2,-transition_log[1][0])<<'\t';
    transition_log[1][1] = -log2((LtoL+1.0)/(LtoB+LtoL+LtoM+3.0));
    cout<<pow(2,-transition_log[1][1])<<'\t';
    transition_log[1][2] = -log2((MtoL+1.0)/(MtoB+MtoL+MtoM+3.0));
    cout<<pow(2,-transition_log[1][2])<<endl;
    transition_log[2][0] = -log2((BtoM+1.0)/(BtoB+BtoL+BtoM+3.0));
    cout<<pow(2,-transition_log[2][0])<<'\t';
    transition_log[2][1] = -log2((LtoM+1.0)/(LtoB+LtoL+LtoM+3.0));
    cout<<pow(2,-transition_log[2][1])<<'\t';
    transition_log[2][2] = -log2((MtoM+1.0)/(MtoB+MtoL+MtoM+3.0));
    cout<<pow(2,-transition_log[2][2])<<endl;
  
    // Update Sensory Model
    int HonB=0,HonL=0,HonM=0,TonB=0,TonL=0,TonM=0;
    for (int i=0; i<observations.size(); i++) {
      if (observations[i] == 'H' && original[i+1] == 0)
        HonB++;
      else if (observations[i] == 'H' && original[i+1] == 1)
        HonL++;
      else if (observations[i] == 'H' && original[i+1] == 2)
        HonM++;
      else if (observations[i] == 'T' && original[i+1] == 0)
        TonB++;
      else if (observations[i] == 'T' && original[i+1] == 1)
        TonL++;
      else if (observations[i] == 'T' && original[i+1] == 2)
        TonM++;
    }
    cout<<endl<<"Sensory probabilities learned:"<<endl;
    sensory[0][0] = -log2((HonB+1.0)/(HonB+TonB+2.0));
    sensory[0][1] = -log2((TonB+1.0)/(HonB+TonB+2.0));
    cout<<pow(2,-sensory[0][0])<<endl;//<<' '<<pow(2,-sensory[0][1])<<endl;
    sensory[1][0] = -log2((HonL+1.0)/(HonL+TonL+2.0));
    sensory[1][1] = -log2((TonL+1.0)/(HonL+TonL+2.0));
    cout<<pow(2,-sensory[1][0])<<endl;//<<' '<<pow(2,-sensory[1][1])<<endl;
    sensory[2][0] = -log2((HonM+1.0)/(HonM+TonM+2.0));
    sensory[2][1] = -log2((TonM+1.0)/(HonM+TonM+2.0));
    cout<<pow(2,-sensory[2][0])<<endl;//<<' '<<pow(2,-sensory[2][1])<<endl;
  
  
  
    // Viterbi
    vit[0][0] = -log2(1.0/3.0);
    vit[1][0] = -log2(1.0/3.0);
    vit[2][0] = -log2(1.0/3.0);
//    cout<<endl<<"Backtracking:"<<endl;
    for (int i=1; i<=observations.size(); i++) {
      for (int j=0; j<3; j++) {
        double min, kurrent, min_back=0;
        for (int k=0; k<3; k++) {
          if (k==0)
            min = vit[k][i-1] + transition_log[j][k];
          kurrent = vit[k][i-1] + transition_log[j][k];
          if (kurrent < min) {
            min = kurrent;
            min_back = k;
          }
        }
        if (observations[i-1]=='H') {
          min += sensory[j][0];
        } else {
          min += sensory[j][1];
        }
        vit[j][i] = min;
//        cout<<min_back<<"\t";
        backtrack[j][i]=min_back;
      }
//      cout<<endl;
    }
//    cout<<endl<<"Viterbi:"<<endl;
    for (int i=0; i<=observations.size(); i++) {
      for (int j=0; j<3; j++) {
//        cout<<vit[j][i]<<' ';
      }
//      cout<<endl;
    }
//    cout<<endl;
    minimum_end = 0;
    for (int i=1; i<3; i++) {
      if (vit[i][observations.size()] < vit[minimum_end][observations.size()])
        minimum_end = i;
    }
    forward=minimum_end;
    backtrack_v.clear();
    fortrack.clear();
    backtrack_v.push_back(minimum_end);
    for (int i=observations.size(); i>0 ; i--) {
      forward = backtrack[forward][i];
      backtrack_v.push_back(forward);
    }
//    cout<<"Sequence of States"<<endl;
    for (int state_i=1; state_i<=observations.size(); state_i++) {
      if (i == iter-1) {
        if (backtrack_v.back() == 0)
          output_file<<'B';
        else if (backtrack_v.back() == 1)
          output_file<<'L';
        else if (backtrack_v.back() == 2)
          output_file<<'M';
      }
      fortrack.push_back(backtrack_v.back());
      backtrack_v.pop_back();
    }
    fortrack.push_back(minimum_end);
    if (i == iter-1) {
      if (fortrack.back() == 0)
        output_file<<'B';
      else if (fortrack.back() == 1)
        output_file<<'L';
      else if (fortrack.back() == 2)
        output_file<<'M';
    }
//    cout<<endl;
  }

  // Find accuracy
  // Question: Isn't this finding the accuracy of original prediction, not
  // classifier?
  int right=0;
  for (int i=0; i<og.size(); i++) {
    if (og[i] == fortrack[i+1])
      right++;
  }
  cout<<endl<<"Accuracy: "<<((double)right/og.size())*100.0<<"%"<<endl;

  output_file.close();
}
